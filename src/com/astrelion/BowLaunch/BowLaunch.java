package com.astrelion.BowLaunch;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class BowLaunch extends JavaPlugin implements Listener
{

	private List<Player> canRide = new ArrayList<Player>();
	private List<Player> time = new ArrayList<Player>();
	private List<Player> riding = new ArrayList<Player>();

	private int cooldownTime;

	private boolean alertMessage
	, timeAlertMessage,
	relaunchMessage,
	ridingMessage;

	private String relaunchMessageS,
	ridingMessageS,
	noPermMessage;

	public void onEnable()
	{

		getServer().getPluginManager().registerEvents(this, this);

		saveDefaultConfig();

		//CONFIG VALUES
		cooldownTime = getConfig().getInt("Cooldown_Time");

		alertMessage = getConfig().getBoolean("Enable_Alert_Message");
		timeAlertMessage = getConfig().getBoolean("Enable_Time_Alert_Message");
		relaunchMessage = getConfig().getBoolean("Enable_ReLaunch_Message");
		ridingMessage = getConfig().getBoolean("Enable_Riding_Message");

		relaunchMessageS = getConfig().getString("ReLaunch_Message").replaceAll("&", "�");
		ridingMessageS = getConfig().getString("Riding_Arrow_Message").replaceAll("&", "�");
		noPermMessage = getConfig().getString("No_Perm_Message").replaceAll("&", "�");

	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{

		//BOWLAUNCH BASE COMMAND
		if(label.equalsIgnoreCase("bowlaunch"))
		{

			Player p = (Player) sender;

			if(canRide.contains(p) && p.hasPermission("bowlaunch.launch"))
			{

				canRide.remove(p);

				if(alertMessage)
				{

					p.sendMessage(ChatColor.BLUE + "You disabled arrow riding for yourself!");
					p.sendMessage(ChatColor.GRAY + "Do /bowlaunch again to enable");

				}

			}
			else if (!canRide.contains(p) && p.hasPermission("bowlaunch.launch"))
			{

				canRide.add(p);

				if(alertMessage)
				{

					p.sendMessage(ChatColor.BLUE + "You enabled arrow riding for yourself!");

				}

			}
			else if (!p.hasPermission("bowlaunch.launch"))
			{

				p.sendMessage(noPermMessage);

			}

		}

		return false;

	}

	@EventHandler
	public void ride(EntityShootBowEvent e)
	{

		if(e.getEntity() instanceof Player)
		{

			Projectile proj = (Projectile) e.getProjectile();
			Player p = (Player) e.getEntity();

			if(p.hasPermission("bowlaunch.launch") && canRide.contains(p) && !time.contains(p))
			{

				time.add(p);

				Cooldown d = new Cooldown();
				d.p = p;
				new Thread(d).start();

				proj.setPassenger(p);
				riding.add(p);

				if(ridingMessage)
				{

					p.sendMessage(ridingMessageS);

				}

			}
			else if(!p.hasPermission("bowlaunch.launch"))
			{

				p.sendMessage(noPermMessage);

			}
			else if(time.contains(p) && timeAlertMessage)
			{

				p.sendMessage(ChatColor.RED + "You need to wait " + cooldownTime + " seconds to ride again!");

			}

		}

	}

	@EventHandler
	public void hitGround(ProjectileHitEvent e)
	{

		Projectile proj = e.getEntity();
		Arrow arrow = (Arrow) proj;
		Player p = (Player) arrow.getShooter();

		//PLAYER HIT GROUND > LEAVE VEHICLE
		if(p.hasPermission("bowlaunch.launch") && proj instanceof Arrow && arrow.getShooter() instanceof Player && p.isInsideVehicle() && canRide.contains(p))
		{

			riding.remove(p);

			p.leaveVehicle();
			proj.remove();
			p.sendMessage(ChatColor.RED + "Arrow hit the something!");

		}

	}

	public class Cooldown implements Runnable
	{

		public Player p;

		//COOLDOWN TIMER
		public void run()
		{

			try
			{

				Thread.sleep(cooldownTime * 1000);

			}
			catch(Exception e)
			{

				e.printStackTrace();

			}

			if(relaunchMessage)
			{

				p.sendMessage(relaunchMessageS);

			}

			time.remove(p);

		}

	}

}
