# [New & improved version here: https://gitlab.com/ASTRELION/launchme](https://gitlab.com/ASTRELION/launchme)

# BowLaunch

Minecraft plugin for riding arrows you fire!

Bukkit page: https://dev.bukkit.org/projects/bowlaunch
